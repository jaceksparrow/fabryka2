package main;

import computer.Computer;
import factories.ComputersFactory;

public class ComputerShop {

    public static void main(String[] args) {
        ComputersFactory factory = new ComputersFactory();
        System.out.println("Podzespoly_dla_laptopa\n");
        Computer laptop = factory.issueComputer("Laptop");
        System.out.println("============================");
        System.out.println("Podzespoly_dla_komputera_stacjonarnego\n");
        Computer komputer = factory.issueComputer("Komputer_stacjonarny");

    }

}
