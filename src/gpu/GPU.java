package gpu;

public interface GPU {

    public GPU installGPU();

    public double getPrice();

}
