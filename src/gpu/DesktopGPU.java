package gpu;

public class DesktopGPU implements GPU {

    String name = "AMD_RADEON_HD_7570";
    double price = 1508.99d;

    @Override
    public GPU installGPU() {

        System.out.print(" | GPU: " + name);
        return new DesktopGPU();
    }

    @Override
    public double getPrice() {
        return this.price;
    }

}
