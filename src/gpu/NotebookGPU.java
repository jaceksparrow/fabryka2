package gpu;

public class NotebookGPU implements GPU {

    String name = "NVIDIA_GTX_760TI";
    double price = 1800.0d;

    @Override
    public GPU installGPU() {

        System.out.print(" | GPU: " + name);
        return new NotebookGPU();
    }

    @Override
    public double getPrice() {
        return this.price;
    }

}
