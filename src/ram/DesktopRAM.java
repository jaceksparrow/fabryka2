package ram;

public class DesktopRAM implements RAM {

    String name = "16GB RAM";
    double price = 200.0d;

    @Override
    public RAM installRAM() {

        System.out.print(" | RAM: " + name);
        return new DesktopRAM();
    }

    @Override
    public double getPrice() {
        return this.price;
    }

}
