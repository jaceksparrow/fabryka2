package ram;

public class NotebookRAM implements RAM {

    String name = "16GB RAM";
    double price = 249.0d;

    @Override
    public RAM installRAM() {

        System.out.print(" | RAM: " + name);
        return new NotebookRAM();
    }

    @Override
    public double getPrice() {
        return this.price;
    }

}
