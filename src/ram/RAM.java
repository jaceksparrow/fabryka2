package ram;

public interface RAM {

    public RAM installRAM();

    public double getPrice();
}
