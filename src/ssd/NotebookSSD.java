package ssd;

public class NotebookSSD implements SSD {

    String name = "Dysk 250GB SSD";
    double price = 500.0d;

    @Override
    public SSD installSSD() {

        System.out.println(" | Dysk: " + name);
        return new NotebookSSD();
    }

    @Override
    public double getPrice() {
        return this.price;
    }

}
