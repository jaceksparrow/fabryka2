package ssd;

public class DesktopSSD implements SSD {

    String name = "Dysk 500GB SSD";
    double price = 700.0d;

    @Override
    public SSD installSSD() {

        System.out.println(" | Dysk: " + name);
        return new DesktopSSD();
    }

    @Override
    public double getPrice() {
        return this.price;
    }

}
