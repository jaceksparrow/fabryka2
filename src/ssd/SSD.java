package ssd;

public interface SSD {

    public SSD installSSD();

    public double getPrice();

}
