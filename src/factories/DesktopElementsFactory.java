package factories;

import cpu.CPU;
import cpu.DesktopCPU;
import gpu.DesktopGPU;
import gpu.GPU;
import ssd.DesktopSSD;
import mainboard.DesktopMainboard;
import mainboard.Mainboard;
import ram.DesktopRAM;
import ram.RAM;
import ssd.SSD;

public class DesktopElementsFactory implements ComputerElementsFactory {

    @Override
    public Mainboard startMainboardProduction() {
        return new DesktopMainboard();
    }

    @Override
    public CPU startCPUProduction() {
        return new DesktopCPU();
    }

    @Override
    public GPU startGPUProduction() {
        return new DesktopGPU();
    }

    @Override
    public RAM startRAMProduction() {
        return new DesktopRAM();
    }

    @Override
    public SSD startSSDProduction() {
        return new DesktopSSD();
    }

}
