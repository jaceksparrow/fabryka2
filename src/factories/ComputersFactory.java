package factories;

import computer.Computer;
import computer.Notebook;
import computer.PC;

public class ComputersFactory {

    public Computer issueComputer(String model) {
        Computer computer = assemblyComputer(model);
        computer.installOS();
        computer.pack();
        computer.sell();
        return computer;
    }

    public Computer assemblyComputer(String model) {
        Computer computer = null;
        DesktopElementsFactory desktopElements = null;
        if (model.equalsIgnoreCase("Komputer_stacjonarny")) {
            computer = new PC(new DesktopElementsFactory());

            // desktopElements = new DesktopElementsFactory();
            // computer = new PC(desktopElements);
        } else if (model.equalsIgnoreCase("Laptop")) {
            computer = new Notebook(new NotebookElementsFactory());
        }
        return computer;
    }
}
