package factories;

import cpu.CPU;
import cpu.NotebookCPU;
import gpu.GPU;
import gpu.NotebookGPU;
import ssd.NotebookSSD;
import mainboard.Mainboard;
import mainboard.NotebookMainboard;
import ram.NotebookRAM;
import ram.RAM;
import ssd.SSD;

public class NotebookElementsFactory implements ComputerElementsFactory {

    @Override
    public Mainboard startMainboardProduction() {
        return new NotebookMainboard();
    }

    @Override
    public CPU startCPUProduction() {
        return new NotebookCPU();
    }

    @Override
    public GPU startGPUProduction() {
        return new NotebookGPU();
    }

    @Override
    public RAM startRAMProduction() {
        return new NotebookRAM();
    }

    @Override
    public SSD startSSDProduction() {
        return new NotebookSSD();
    }

}
