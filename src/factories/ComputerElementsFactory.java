package factories;

import cpu.CPU;
import gpu.GPU;
import mainboard.Mainboard;
import ram.RAM;
import ssd.SSD;

public interface ComputerElementsFactory {

    public Mainboard startMainboardProduction();

    public CPU startCPUProduction();

    public GPU startGPUProduction();

    public RAM startRAMProduction();

    public SSD startSSDProduction();

}
