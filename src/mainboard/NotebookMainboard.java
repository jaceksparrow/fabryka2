package mainboard;

public class NotebookMainboard implements Mainboard {

    String name = "D630_nVidia_REFUR_R872J";
    double price = 855.0d;

    @Override
    public Mainboard installMainboard() {

        System.out.print(" Plyta_glowna: " + name);
        return new NotebookMainboard();
    }

    @Override
    public double getPrice() {
        return this.price;
    }

}
