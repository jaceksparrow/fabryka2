package mainboard;

public interface Mainboard {

    public Mainboard installMainboard();

    public double getPrice();

}
