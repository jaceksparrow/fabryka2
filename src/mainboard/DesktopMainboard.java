package mainboard;

public class DesktopMainboard implements Mainboard {

    String name = "MSI_Z170A";
    double price = 385.0d;

    @Override
    public Mainboard installMainboard() {

        System.out.print(" Plyta_glowna: " + name);
        return new DesktopMainboard();
    }

    @Override
    public double getPrice() {
        return this.price;
    }
}
