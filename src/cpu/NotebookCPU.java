package cpu;

public class NotebookCPU implements CPU {

    String name = "i7-8650U";
    double price = 649.49d;

    @Override
    public CPU installCPU() {

        System.out.print(" | CPU: " + name);
        return new NotebookCPU();
    }

    @Override
    public double getPrice() {
        return this.price;
    }

}
