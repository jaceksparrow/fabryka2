package cpu;

public class DesktopCPU implements CPU {

    String name = "i5-8400";
    double price = 1100.0d;

    @Override
    public CPU installCPU() {

        System.out.print(" | CPU: " + name);
        return new DesktopCPU();
    }

    @Override
    public double getPrice() {
        return this.price;
    }

}
