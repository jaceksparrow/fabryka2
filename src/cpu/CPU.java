package cpu;

public interface CPU {

    public CPU installCPU();

    public double getPrice();

}
