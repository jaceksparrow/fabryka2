package computer;

import cpu.CPU;
import gpu.GPU;
import mainboard.Mainboard;
import ram.RAM;
import ssd.SSD;

public abstract class Computer {

    Mainboard mainboard;
    CPU cpu;
    GPU gpu;
    RAM ram;
    SSD ssd;

    public abstract void assembly();

    public abstract void showTotalPrice();

    public void installOS() {
        System.out.println(" Instalacja systemu operacyjnego oraz oprogramowania");
    }

    public void pack() {
        System.out.println(" Pakowanie");
    }

    public void sell() {
        System.out.println(" Sprzedawanie");
    }

}
