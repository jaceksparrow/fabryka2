package computer;

import cpu.NotebookCPU;
import factories.NotebookElementsFactory;
import gpu.NotebookGPU;
import ssd.NotebookSSD;
import mainboard.NotebookMainboard;
import ram.NotebookRAM;

public class Notebook extends Computer {

    NotebookElementsFactory notebookElementsFactory;
    NotebookMainboard mbNB;
    NotebookCPU cpuNB;
    NotebookGPU gpuNB;
    NotebookRAM ramNB;
    NotebookSSD ssdNB;

    public Notebook(NotebookElementsFactory notebookElementsFactory) {
        this.notebookElementsFactory = notebookElementsFactory;
        assembly();
        showTotalPrice();
    }

    @Override
    public void assembly() {
        mainboard = notebookElementsFactory.startMainboardProduction().installMainboard();
        cpu = notebookElementsFactory.startCPUProduction().installCPU();
        gpu = notebookElementsFactory.startGPUProduction().installGPU();
        ram = notebookElementsFactory.startRAMProduction().installRAM();
        ssd = notebookElementsFactory.startSSDProduction().installSSD();
    }

    @Override
    public void showTotalPrice() {
        mbNB = new NotebookMainboard();
        cpuNB = new NotebookCPU();
        gpuNB = new NotebookGPU();
        ramNB = new NotebookRAM();
        ssdNB = new NotebookSSD();
        double totalPriceforKit = mbNB.getPrice() + cpuNB.getPrice() + gpuNB.getPrice() + ramNB.getPrice()
                + ssdNB.getPrice();
        System.out.println(" Cena: " + totalPriceforKit + " zloty.");
    }

}
