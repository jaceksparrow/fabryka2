package computer;

import cpu.DesktopCPU;
import factories.DesktopElementsFactory;
import gpu.DesktopGPU;
import ssd.DesktopSSD;
import mainboard.DesktopMainboard;
import ram.DesktopRAM;

public class PC extends Computer {

    DesktopElementsFactory desktopElementsFactory;
    DesktopMainboard mbDT;
    DesktopCPU cpuDT;
    DesktopGPU gpuDT;
    DesktopRAM ramDT;
    DesktopSSD hddDT;

    public PC(DesktopElementsFactory desktopElementsFactory) {
        this.desktopElementsFactory = desktopElementsFactory;
        assembly();
        showTotalPrice();
    }

    @Override
    public void assembly() {
        mainboard = desktopElementsFactory.startMainboardProduction().installMainboard();
        cpu = desktopElementsFactory.startCPUProduction().installCPU();
        gpu = desktopElementsFactory.startGPUProduction().installGPU();
        ram = desktopElementsFactory.startRAMProduction().installRAM();
        ssd = desktopElementsFactory.startSSDProduction().installSSD();
    }

    @Override
    public void showTotalPrice() {
        mbDT = new DesktopMainboard();
        cpuDT = new DesktopCPU();
        gpuDT = new DesktopGPU();
        ramDT = new DesktopRAM();
        hddDT = new DesktopSSD();
        double totalPriceforKit = mbDT.getPrice() + cpuDT.getPrice() + gpuDT.getPrice() + ramDT.getPrice()
                + hddDT.getPrice();
        System.out.println(" Cena: " + totalPriceforKit + " zloty.");
    }

}
